import Vue from 'vue'
import VueRouter from 'vue-router'

const Home = () => import('../views/Home.vue');
const EnterpriseManage = () => import('../views/EnterpriseManage.vue');
const InfoRelease = () => import('../views/InfoRelease.vue');
const User = () => import('../views/User.vue');
const HomeMain = () => import('../components/home/HomeMain.vue');
const InfoDetails = () => import('../components/home/InfoDetails.vue')

Vue.use(VueRouter)
const originalReplace = VueRouter.prototype.replace;
VueRouter.prototype.replace = function replace(location) {
  return originalReplace.call(this, location).catch(err => err);
};
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  { path: '/', redirect: '/home' },
  {
    name: 'home',
    path: '/home',
    component: Home,
    redirect: '/infolist',
    children: [
      {
        name: 'infoList',
        path: '/infolist',
        component: HomeMain
      }, {
        name: 'infoDetails',
        path: '/infolist/details/:infoid',
        component: InfoDetails
      }
    ]
  },
  {
    path: '/release',
    component: InfoRelease,
    name: 'release',
    beforeEnter(to, from, next) {
      if (window.sessionStorage.getItem('loginStatus')) {
        if (window.sessionStorage.getItem('usreIdentity') === 'enterprise') {
          next();
        } else {
          alert('企业用户专享模块~')
        }
      } else {
        next(from.path)
        alert('请先登录~')
      }
    }
  },
  {
    path: '/manage',
    component: EnterpriseManage,
    name: 'manage',
    beforeEnter(to, from, next) {
      if (window.sessionStorage.getItem('loginStatus')) {
        if (window.sessionStorage.getItem('usreIdentity') === 'enterprise') {
          next();
        } else {
          alert('企业用户专享模块~')
        }
      } else {
        next(from.path)
        alert('请先登录~')
      }
    }
  },
  {
    path: '/user',
    component: User,
    name: 'user',
    beforeEnter: (to, from, next) => {
      if (window.sessionStorage.getItem('loginStatus')) {
        if (window.sessionStorage.getItem('usreIdentity') === 'student') {
          next();
        } else {
          alert('学生用户专享模块~')
        }
      } else {
        next(from.path)
        alert('请先登录~')
      }
    }
  },
]

const router = new VueRouter({
  routes
})

// router.beforeEach((to, from, next) => {
//   if (to.meta.isAuth) {
//     if (window.sessionStorage.getItem('loginStatus')) {
//       if (window.sessionStorage.getItem('usreIdentity') === 'student') {
//         next()
//       } else {
//         alert('学生用户专属~')
//       }
//     } else {
//       alert('请先登录~')
//     }
//   } else {
//     next()
//   }
// })
export default router
