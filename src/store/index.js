import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isShowHomeTab: false,
    infoList: [],
    // userId: null,
    // infoDetailsTop: {},
    // infoDetails: {}
  },
  getters: {
  },
  mutations: {
    changeIsShowTab(state) {
      state.isShowHomeTab = !state.isShowHomeTab
    },
    GetNewInfoList(state, list) {
      // 拼接数据
      state.infoList.push(...list);
    },
    initializationInfoList(state) {
      state.infoList = []
    },
    // changeIsShowLastList(state) {
    //   state.isShowLastList = !state.isShowLastList
    // },
    // getInfoDetailsTop(state, obj) {
    //   state.infoDetailsTop = obj;
    // },
    // getInfoDetailsVuex(state, obj) {
    //   state.infoDetails = obj;
    // },
    // getUserId(state, id) {
    //   state.userId = id
    // }
  },
  actions: {
  },
  modules: {
  }
})
