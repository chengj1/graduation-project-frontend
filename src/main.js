import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入全局样式
import './assets/css/normalize.css'
import './assets/css/global.css'
// 引入 element-ui
import './plugin/element'
// 引入 滚动组件
import infiniteScroll from "vue-infinite-scroll"
// 引入 axios 
import axios from 'axios'
// 引入 qs
import qs from 'qs';


// 默认配置
axios.defaults.baseURL = 'http://127.0.0.1:6606/';
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
Vue.prototype.$http = axios;
Vue.prototype.$qs = qs;
Vue.use(infiniteScroll)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
