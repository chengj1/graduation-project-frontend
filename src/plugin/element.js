import Vue from 'vue';
import { Message, MessageBox, Menu, MenuItem, Dropdown, DropdownMenu, DropdownItem, Input, Tag, PageHeader, Descriptions, DescriptionsItem, Card, Button, Backtop, Dialog, Form, FormItem, Select, Option, DatePicker,Steps, Step, InputNumber, Upload,  Tabs, TabPane,Empty} from 'element-ui'

Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(Dropdown)
Vue.use(DropdownMenu)
Vue.use(DropdownItem)
Vue.use(Input)
Vue.use(Tag)
Vue.use(PageHeader)
Vue.use(Descriptions)
Vue.use(DescriptionsItem)
Vue.use(Card)
Vue.use(Button)
Vue.use(Backtop)
Vue.use(Dialog)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Select)
Vue.use(Option)
Vue.use(DatePicker)
Vue.use(Steps)
Vue.use(Step)
Vue.use(InputNumber)
Vue.use(Upload)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Empty)

Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm