// 时间格式化
function timeFormatting(date) {
  let tdate = new Date(date);
  let yy = tdate.getFullYear();
  let mm = tdate.getMonth() + 1;
  mm = mm > 9 ? mm : '0' + mm;
  let dd = tdate.getDate();
  dd = dd > 9 ? dd : '0' + dd;
  let h = tdate.getHours();
  h = h > 9 ? h : '0' + h;
  let m = tdate.getMinutes();
  m = m > 9 ? m : '0' + m;
  let s = tdate.getSeconds();
  s = s > 9 ? s : '0' + s;
  return `${yy}-${mm}-${dd} ${h}:${m}:${s}`;
}
export { timeFormatting }