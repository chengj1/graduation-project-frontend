### 安装依赖
```
npm install
```

### 本地运行
```
npm run serve
```

### 打包
```
npm run build
```

### 运行界面概览

首页：

[![Lgi0Qs.png](https://s1.ax1x.com/2022/04/21/Lgi0Qs.png)](https://imgtu.com/i/Lgi0Qs)

资讯详情：

[![Lgi2YF.png](https://s1.ax1x.com/2022/04/21/Lgi2YF.png)](https://imgtu.com/i/Lgi2YF)

登录：

[![Lgise0.png](https://s1.ax1x.com/2022/04/21/Lgise0.png)](https://imgtu.com/i/Lgise0)

注册：

[![Lgi6oT.png](https://s1.ax1x.com/2022/04/21/Lgi6oT.png)](https://imgtu.com/i/Lgi6oT)

个人中心：

[![LgiywV.png](https://s1.ax1x.com/2022/04/21/LgiywV.png)](https://imgtu.com/i/LgiywV)

企业管理1：

[![LgiDLq.png](https://s1.ax1x.com/2022/04/21/LgiDLq.png)](https://imgtu.com/i/LgiDLq)

企业管理2：

[![LgiByn.png](https://s1.ax1x.com/2022/04/21/LgiByn.png)](https://imgtu.com/i/LgiByn)

资讯发布：

[![LgigFU.png](https://s1.ax1x.com/2022/04/21/LgigFU.png)](https://imgtu.com/i/LgigFU)

